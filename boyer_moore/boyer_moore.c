#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int **CreerTableDecalage(char *motif, int taille);
void AfficheTableDecalage(int **decalage, int taille);
int BoyerMoore(char *motif, char *texte);


const int CHAR_AMOUNT = 256;


int **CreerTableDecalage(char *motif, int taille) {
	// Entrée : une chaîne de caractère et un entier correspondant à sa taille
	// Sortie : la table de décalage du motif

	// creation de la table de décalage de taille : (taille du motif) * ascii.
	int **table_decalage = malloc(sizeof(int*) * CHAR_AMOUNT * taille);
	assert(table_decalage != NULL);

	// creation d'un tableau de 256 cases pour les 256s caractères possible dans
	// le motif.
	int *ascii = malloc(sizeof(int) * CHAR_AMOUNT);
	assert(ascii != NULL);

	// complète la première ligne de décalage. -1 correspondra à l'absence de
	// décalage dû à une lettre dans le motif.
	for (int i = 0; i < CHAR_AMOUNT; i++) {
		ascii[i] = -1;
	}
	table_decalage[0] = ascii;

	// remplissage de la table de décalage
	for (int i = 1; i < taille; i++) {

		// création d'une nouvelle ligne
		int *ascii = malloc(sizeof(int) * CHAR_AMOUNT);
		assert(ascii != NULL);

		// copiage de la ligne précédente dans la ligne suivante.
		for (int j = 0; j < CHAR_AMOUNT; j++) {
			ascii[j] = table_decalage[i - 1][j];
		}

		// on rapporte l'indice de la lettre i dans le tableau ascii.

		ascii[(int) motif[i - 1] % CHAR_AMOUNT] = i - 1;
		table_decalage[i] = ascii;
	}
	return table_decalage;
}

void AfficheTableDecalage(int **decalage, int taille) {
	// Entrée : une table de decalage et sa taille
	// Sortie : rien
	// Rôle :  Affiche la table de decalage.

	printf("Affichage de la table de decalage :\n[");
	for (int i = 0; i < taille; i++) {
		for (int j = 0; j < CHAR_AMOUNT; j++) {
			printf("\t%d", decalage[i][j]);
		}
		printf("]\n[");
	}
}

int BoyerMoore(char *motif, char *texte) {
	// Entree : deux chaines de caractères, motif et texte.
	// Sortie : le nombre  d'occurrences du motif dans le texte.
	// Affiche la position des occurrences du motif dans le texte.

	int taille_motif = strlen(motif);
	int taille_texte = strlen(texte);

	int **table_decalage = CreerTableDecalage(motif, taille_motif);

	int nb_occurrences = 0;
	int *tab_occurrences = malloc(sizeof(taille_texte));
	assert(tab_occurrences != NULL);

	int indice_texte = 0;

	// Recherche du motif dans le texte.
	while (indice_texte < taille_texte - taille_motif + 1) {

		bool difference_trouve = false;
		int indice_motif = taille_motif;
		
		// On compare les caractères du motif et du texte.
		while (!difference_trouve && indice_motif > 0) {
			indice_motif--;

			difference_trouve = motif[indice_motif] !=
				texte[indice_texte + indice_motif];
		}

		// Si le motif a bien été trouvé, on le compte
		if (!difference_trouve) {
			tab_occurrences[nb_occurrences] = indice_texte;
			nb_occurrences++;
			indice_texte++;
		} else {
			int decalage = table_decalage[indice_motif]
				[texte[indice_texte + indice_motif] % 256];

			// On décale de la différence entre la position du motif
			// et celle du prochain caractère que l'on veut
			// (marche aussi pour les valeurs de -1)
			indice_texte += indice_motif - decalage;
		}
	}

	// affichage de l'indice des occurrences du motifs dans le texte.
	for (int i = 0; i < nb_occurrences; i++) {
		printf("Occurrence %d : %d\n", i, tab_occurrences[i]);
	}

	return nb_occurrences;
}

int main() {
	// AfficheTableDecalage(CreerTableDecalage("banane", 6), 6);
	printf("Soit, %d occurrences de an dans banane.\n",
				 BoyerMoore("na", "banana"));
	printf("Soit, %d occurrences de pan dans anpanman.\n",
				 BoyerMoore("pan", "anpanman"));
	return 0;
}
