#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

int RechercheNaiveMotif(char* texte,char* motif){
    //Entree: string texte et motif, longueur motif<longueur texte
    //Sortie Nombre d'occurence du motif dans le texte
    int n=strlen(texte);
    int p=strlen(motif);
    int compt=0;
    for(int i=0;i<=n-p;i++){       
        bool test=true;
        int j=0;
        while(j<p && test==true){
            if(texte[i+j]!=motif[j]){
                test=false;
                }
            j++;
        }
        if (test==true){compt++;}
    }
    return compt;
}

int main(){
    char* txt="ANPANMANPAN";
    char* mot="PAN";
    int c=RechercheNaiveMotif(txt,mot);
    printf("Recherche naive: \n");
    printf("Texte: %s\n",txt);
    printf("Motif: %s\n",mot);
    printf("Nombre d'occurence: ");
    printf("%d\n",c);
    return 0;
}