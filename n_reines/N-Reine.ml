let est_valide (list_reine:int list) (ligne:int) (colonne:int) (nb_reine:int):bool =
  let rec aux list l :bool=
   match list with     
    | []-> true
    | x::r-> (abs (ligne - l) != abs (colonne - x)) && (colonne != x) && (aux r (l-1))
  in aux list_reine nb_reine;;

let n_reine (n:int):int list list =
  let solu =ref [] in
  let rec aux (list_reine:int list) (ligne:int):unit =
    if ligne = n then 
      solu := (list_reine :: (!solu))
    else 
      begin
        for j = 0 to n-1 do
          if (est_valide list_reine (ligne+1) j (ligne)) then
            aux (j::list_reine) (ligne+1) ;
        done;
      end
  in aux [] 0;
  !solu;;