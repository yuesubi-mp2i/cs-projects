#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//////////////
// STRUCTURES


typedef size_t* PosReines;

// Tableau dynamique de PosReines
typedef struct {
	size_t longueur;
	size_t capacite;
	PosReines *tableau;
} ListePos;


//////////////
// CONSTANTES


const size_t FACTEUR_GRANDIS = 2;


//////////////
// PROTOTYPES


ListePos ListePositions_creer();
void ListePos_liberer(ListePos *liste);
void ListePos_ajouter(ListePos *liste, PosReines positions);
void TestListePos();

ListePos NReines(size_t n);
bool AjouterReineValide(PosReines pos_reines, size_t nbr_places);
void _DebugAfficherChemin(PosReines positions, size_t profondeur);
void TestNReines();


///////////////////
// LISTE DE POSITIONS


/* Créer une liste l de positions.
 * Sortie : l
 */
ListePos ListePositions_creer()
{
	ListePos liste;
	liste.longueur = 0;
	liste.capacite = 0;
	liste.tableau = NULL;
	return liste;
}


/* Libérer la liste l.
 * Entrée : l
 */
void ListePos_liberer(ListePos *liste)
{
	liste->longueur = 0;
	liste->capacite = 0;
	free(liste->tableau);
}


/* Ajouter les positions des Reines pr à la liste l.
 * Entrées : l, pr
 */
void ListePos_ajouter(ListePos *liste, PosReines positions)
{
	liste->longueur++;

	if (liste->longueur > liste->capacite) {
		liste->capacite = liste->longueur * FACTEUR_GRANDIS;

		PosReines *nouveau_ptr = realloc(
			liste->tableau,
			sizeof(PosReines) * liste->capacite
		);
		assert(nouveau_ptr != NULL);

		liste->tableau = nouveau_ptr;
	}

	liste->tableau[liste->longueur - 1] = positions;
}


/* Tests des listes de positions */
void TestListePos()
{
	const size_t longueur_donnees_test = 2;
	size_t donnees_test[] = { 42, 2024 };

	ListePos liste = ListePositions_creer();

	for (size_t i = 0; i < longueur_donnees_test; i++){
		ListePos_ajouter(&liste, (void *) donnees_test[i]);
	}

	for (size_t i = 0; i < liste.longueur; i++){
		assert(liste.tableau[i] == (void *) donnees_test[i]);
	}
	
	ListePos_liberer(&liste);
}


////////////
// N REINES


/* Algorithme des N Reines. Détermine la liste l de chacune des combinaison de
 * positions possible pour placer N Reines sur un échiquier de taille N.
 * Entrée : N
 * Sortie : l
 */
ListePos NReines(size_t n)
{
	ListePos liste_positions = ListePositions_creer();

	PosReines positions = malloc(n * sizeof(size_t));
	size_t profondeur = 0;
	positions[profondeur] = 0;

	while (positions[0] < n) {
		if (positions[profondeur] >= n) {
			// On est arrivé au bout des possibilités ne remettant
			// pas en cause le choix précédent. Il faut donc le
			// remettre en cause.

			assert(profondeur > 0);
			profondeur--;

			positions[profondeur]++;

		} else if (!AjouterReineValide(positions, profondeur)) {
			positions[profondeur]++;
			
		} else if (profondeur != n - 1) {
			profondeur++;
			positions[profondeur] = 0;

		} else {
			ListePos_ajouter(
				&liste_positions,
				positions
			);

			positions = malloc(n * sizeof(size_t));
			memcpy(
				positions,
				liste_positions.tableau[liste_positions.longueur - 1],
				n * sizeof(size_t)
			);

			positions[profondeur]++;
		}
	}

	free(positions);
	return liste_positions;
}


/* Détermine si l'ajout d'une Reine r sur l'échiquier est compatible avec la
 * les n autres Reines placés aux positions p.
 * Entrées : p, n
 * Pré-conditions :
 * - Pour chaque entier naturel k < n, AjouterReineValide(pr, k) renvoie true.
 * - La position pr[n] est celle de la Reine r.
 * Sortie : true si l'ajout de la Reine est possible, false sinon.
 */
bool AjouterReineValide(PosReines pos, size_t nbr_places)
{
	bool colonne_valide = true;
	bool diag_gauche_valide = true;
	bool diag_droite_valide = true;

	for (size_t i = 0; i < nbr_places; i++) {
		colonne_valide = colonne_valide &&
			pos[i] != pos[nbr_places];
		
		diag_gauche_valide = diag_gauche_valide &&
			nbr_places - i != pos[nbr_places] - pos[i];

		diag_droite_valide = diag_droite_valide &&
			nbr_places - i != pos[i] - pos[nbr_places];
	}

	return colonne_valide && diag_gauche_valide && diag_droite_valide;
}


/* Affiche le chemin choisi lors de l'execution de l'algorithme des N Reines */
void _DebugAfficherChemin(PosReines positions, size_t profondeur)
{
	printf("prof %ld : ", profondeur);
	for (size_t i = 0; i <= profondeur; i++) {
		printf("%ld.", positions[i]);
	}

	printf("\n");
}


/* Test de l'algorithme des N Reines */
void TestNReines()
{
	assert(NReines(0).longueur == 0);
	assert(NReines(1).longueur == 1);
	assert(NReines(2).longueur == 0);

	assert(NReines(8).longueur == 92);
}


//////////////////////////////
// POINT D'ENTRÉE DU PROGRAMME


int main()
{
	TestListePos();
	// TestEspOccupe();
	TestNReines();
	return 0;
}