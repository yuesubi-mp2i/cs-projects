#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "fusion.h"
#include "nuage.h"


int main()
{
    srand(time(NULL)); 
    point *nuage_point=NuageGenereAleatoire(100);
    NuageAffiche(nuage_point,100);

    printf("\n\nSORTED\n");
    point *sorted = FusionSort(nuage_point, 100);
    NuageAffiche(sorted, 100);

    return 0;
}
