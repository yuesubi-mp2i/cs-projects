// Algo rabin-karp
// En groupe 4
#include <assert.h> 
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



size_t CharPositif(char caractere);
size_t Puissance(size_t x, size_t n, size_t m);

size_t Hachage(const char *motif, size_t taille_modif);
size_t HachageProchain(size_t hach_precedent, char char_supprime,
	char char_ajoute, size_t base_lm);

size_t RabinKarp(char* motif, char* texte);

char *RecupererTexteFichier(const char *nom_fichier);


const size_t modulo = 1869461003;
const size_t base = 256;


/* Renvoie un encodage positif e d'un caractère c.
 * Entrée : c
 * Sortie : e
 * Post-condition : 0 <= e < 256
 */
size_t CharPositif(char caractere)
{
	return (unsigned char) caractere;
}


/* Calcul de x puissance n le tout modulo m.
 * Entrées : x, n, m
 * Sortie : le résultat
 */
size_t Puissance(size_t x, size_t n, size_t m)
{
	size_t x_modulo_m = x % m;

	if (n == 0) {
		return 1;
	} else if (n % 2 == 1) {
		size_t temp = Puissance(x, (n - 1) / 2, m);
		return (((temp * temp) % m) * x_modulo_m) % m;
	} else {
		size_t temp = Puissance(x, n / 2, m);
		return (temp * temp) % m;
	}
}


/* Calcule le hash h d'un texte motif de taille taille_motif.
 * Entrées : motif, taille_motif
 * Sortie : h
 */
size_t Hachage(const char *motif, size_t taille_motif)
{
	size_t hash = 0;

	for (size_t i = 0; i < taille_motif; i++) {
		hash *= base;
		hash %= modulo;
		hash += CharPositif(motif[i]);
		hash %= modulo;
	}

	return hash;
}


/* Calcule le hash h d'un texte t de longueur n, formé à partir d'un texte t',
 * de hash hash_precedent, en retirant char_supprime à gauche et en ajoutant
 * char_ajoute à droite.
 * Entrées : hash_precedent, char_supprime, char_ajoute, base_dom
 * Sortie : h
 * Pré-conditions :
 * 	- base_dom = base^(n - 1) [modulo]
 * 	- hash_precedent = Hachage(t', n)
 * Post-condition : h = Hachage(t, n)
 */
size_t HachageProchain(
	size_t hash_precedent,
	char char_supprime,
	char char_ajoute,
	size_t base_dom
) {
	size_t a_retirer = (base_dom * (CharPositif(char_supprime) % modulo)) % modulo;
	//printf("\t-> %d\n", a_retirer);
	size_t hash_reduit = (base * (hash_precedent - a_retirer)) % modulo;
	//printf("\t-> %d\n", hach_reduit);
	size_t hash = (hash_reduit + CharPositif(char_ajoute) % modulo) % modulo;
	//printf("\t-> %d | %d\n", hach, char_ajoute);
	return hash;
	// return (base * (hach_precedent - (base_lm * char_supprime)%modulo) + char_ajoute) % modulo;
}


/* NON TESTÉ !
 * Recherche du nombre d'occurrences p d'un motif motif dans un texte texte
 * avec l'algorithme de Rabin-Karp.
 * Entrées : motif, texte
 * Sortie : p
 */
size_t RabinKarp(char* motif, char* texte){

	size_t taille_motif = strlen(motif);
	size_t taille_texte = strlen(texte);

	size_t hachage_motif = Hachage (motif, taille_motif);
	size_t hachage_texte = Hachage( texte, taille_motif);

	size_t occurrences = 0;
	size_t base_dom = Puissance(base, taille_motif - 1, modulo);


	for (size_t i = 0; i <= taille_texte - taille_motif; i++)
	{
		if (hachage_texte == hachage_motif){
			bool motif_trouve = true;

			for (size_t j = 0; j < taille_motif; j++) {
				motif_trouve = motif_trouve &&
					texte[i + j] == motif[j];
			}

			if (motif_trouve){
				occurrences ++;
				printf(" %d : Motif trouve a l'indice %d \n", occurrences, i);
			}

			// printf("  %d %d ->", compteur, taille_motif);
			// for (int j = -15; j < taille_motif+15; j++) { printf("%c", texte[i+j]); }
			// printf("\n");
			
		} 

		// printf("%d:%c -> %d v. %d\n", i, texte[i], hachage_texte, Hachage(&texte[i], taille_motif));
		// assert(hachage_texte == Hachage(&texte[i], taille_motif));

		hachage_texte = HachageProchain(
			hachage_texte,
			texte[i], texte[taille_motif + i],
			base_dom
		);
				
	}
	
	return occurrences;
}


/* Renvoie le contenu d'un fichier nom_fichier sous force de chaîne de
 * caractère cdc.
 * Entrée : nom_fichier
 * Pré-condition : le fichier nom_fichier doit exister
 * Sortie : cdc
 */
char *RecupererTexteFichier(const char *nom_fichier)
{
	FILE *f;
	f = fopen(nom_fichier, "r");
	assert(f != NULL);

	char c;
	size_t taille_texte = 0;

	while ((c = fgetc(f)) != EOF){
		taille_texte++;
	}

	fclose(f);

	char *texte = malloc(taille_texte + 1);
	assert(texte != NULL);
	
	f = fopen(nom_fichier, "r");
	assert(f != NULL);

	for (size_t i = 0; i < taille_texte; i++){
		texte[i] = fgetc(f);
	}
	fclose(f);

	texte[taille_texte] = '\0';
	return texte;
}


int main()
{
	char *texte = RecupererTexteFichier("Spinoza.txt");
	char *motif = "libre";

	printf("%s : %d\n", motif, RabinKarp(motif, texte));

	motif = "aaa";
	printf("%s : %d\n", motif, RabinKarp(motif, "aaaÉaaaa"));

	return 0;
}